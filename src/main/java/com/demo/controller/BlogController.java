package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.demo.service.TestService;
import com.jfinal.core.Controller;

public class BlogController extends Controller {
	@Autowired
	private TestService service;
 
	@Autowired
	public void setApplicationContext(ApplicationContext ctx) {
		System.out.println("HelloController attr[ApplicationContext] 已出入。。");
	}

	public void index() {
		service.update();
		renderText("Hello JFinal World.");
	}

	public void t() {
		renderText("Hello JFinal World.");
	}
}
