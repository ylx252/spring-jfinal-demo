package com.demo.service.impl;

import org.springframework.stereotype.Service;

import com.demo.model.BlogModel;
import com.demo.service.TestService;

@Service
public class TestServiceImpl implements TestService {
	@Override
	public void update() {
		BlogModel.dao.findById("1").set("title", "n11").update();
		System.out.println("---");
	} 
}
