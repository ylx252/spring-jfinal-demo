package com.demo.test;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import cc.ecore.spring.jfinal.javassist.CtFactory;

import com.demo.controller.BlogController;
import com.demo.model.BlogModel;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.AnsiSqlDialect;
import com.jfinal.render.ViewType;

public class CommonConfig extends JFinalConfig {

	private ApplicationContext context;

	@Autowired
	public void setApplicationContext(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public void configConstant(Constants me) {
		System.out.println("configConstant 缓存 properties");
		loadPropertyFile("common.properties");

		System.out.println("configConstant 设置字符集");
		me.setEncoding("UTF-8");

		System.out.println("configConstant 设置是否开发模式");
		me.setDevMode(getPropertyToBoolean("devMode", true));
		me.setViewType(ViewType.JSP);
	}

	@Override
	public void configRoute(Routes me) {
		System.out.println("configRoute");
		me.add("/blog", BlogController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		Boolean devMode = getPropertyToBoolean("devMode", true);// 开发模式

		DataSource dataSource = context.getBean("dataSourceProxy", DataSource.class);

		System.out.println("configPlugin 配置ActiveRecord插件");
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dataSource);
		arp.setDevMode(devMode); // 设置开发模式 

		System.out.println("configPlugin 使用数据库类型是 sql server");
		arp.setDialect(new AnsiSqlDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));// 小写
		// mapping
		arp.addMapping("blog", BlogModel.class);

		System.out.println("configPlugin 添加DruidPlugin插件");
		me.add(arp); // 
	}

	@Override
	public void configInterceptor(Interceptors me) {
		System.out.println("configInterceptor");
	}

	@Override
	public void configHandler(Handlers me) {
		System.out.println("configHandler");
	}

	public static void main(String[] args) {
		CtFactory.toClass();
		//直接在这个页面启动，在浏览器中输入localhost/index 进行访问
		JFinal.start("src/main/webapp", 80, "/", 5);
	}
}
