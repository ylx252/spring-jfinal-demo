/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package com.jfinal.core;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import cc.ecore.spring.jfinal.javassist.CtFactory;

/**
 * 用于 lazy jfinalFilter
 */
public class SpringJFinalFilter implements Filter {
	private Filter jfinalFilter;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		try {
			CtFactory.toClass();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		jfinalFilter = createJFinalFilter("com.jfinal.core.JFinalFilter");
		jfinalFilter.init(filterConfig);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		jfinalFilter.doFilter(request, response, chain);
	}

	@Override
	public void destroy() {
		jfinalFilter.destroy();
	}

	private Filter createJFinalFilter(String filterClass) {
		Object temp = null;
		try {
			temp = Class.forName(filterClass).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Can not create instance of class: " + filterClass, e);
		}

		if (temp instanceof Filter) {
			return (Filter) temp;
		} else {
			throw new RuntimeException("Can not create instance of class: " + filterClass + ".");
		}
	}
}
