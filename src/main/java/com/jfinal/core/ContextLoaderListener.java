/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package com.jfinal.core;

import javax.servlet.ServletContextEvent;

import cc.ecore.spring.jfinal.javassist.CtFactory;

/**
 * 重写 spring listener 用于覆盖关键 class
 */
public class ContextLoaderListener extends org.springframework.web.context.ContextLoaderListener  {
 
	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			CtFactory.toClass();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		super.contextInitialized(event);
	}
}
