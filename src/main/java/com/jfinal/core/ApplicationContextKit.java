/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package com.jfinal.core;

import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.FilterConfig;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.Assert;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.jfinal.config.JFinalConfig;

/**
 * spring 工具箱
 */
class ApplicationContextKit {

	static ApplicationContext applicationContext;
  
	/**
	 * 初始化  ApplicationContext
	 */
	static void initApplicationContext(FilterConfig filterConfig) {
		ApplicationContextKit.applicationContext = WebApplicationContextUtils.getWebApplicationContext(filterConfig
				.getServletContext());
		// 断言初始化成功
		Assert.notNull(ApplicationContextKit.applicationContext, "ApplicationContext can not be null");
	}

	/**
	 * 获取 spring bean
	 */
	static <T> T getBean(Class<T> requiredType) {
		// beanClass 不能为空
		Assert.notNull(requiredType, "requiredType can not be null");
		return applicationContext.getBean(requiredType);
	}

	/**
	 * 注册  Controller
	 */
	static void registerController() {
		Set<Entry<String, Class<? extends Controller>>> controllerClasses = Config.getRoutes().getEntrySet();
		for (Entry<String, Class<? extends Controller>> e : controllerClasses) {
			registerController(e.getValue());
		}
	}

	/**
	 * @Title: 向spring容器注册 Controller
	 * @param configClass
	 * @param scope prototype
	 * @return Controller
	 */
	static void registerController(Class<? extends Controller> controllerClass) {
		// beanClass 不能为空
		Assert.notNull(controllerClass, "controllerClass can not be null");
		// 动态注册,每次请求都创建一个实例 符合 jfinal 创建 Controller方式 (jfinal 是每次请求都创建实例).
		registerBean(applicationContext, controllerClass, BeanDefinition.SCOPE_PROTOTYPE);
	}

	/**
	 * @Title:  向spring容器注册 JFinalConfig
	 * @param configClass 
	 * @param scope singleton
	 */
	static JFinalConfig registerJFinalConfig(String configClass) {
		// beanClass 不能为空
		Assert.notNull(configClass, "configClass can not be null");
		// 动态注册,每次请求都创建一个实例 符合 jfinal 创建 Controller方式 (jfinal 是每次请求都创建实例).
		registerBean(applicationContext, configClass, BeanDefinition.SCOPE_SINGLETON);
		return (JFinalConfig) applicationContext.getBean(configClass);
	}

	/**
	 * @Title: 向spring容器注册bean
	 * @param applicationContext
	 * @param beanClass
	 * @param scope 
	 */
	static void registerBean(ApplicationContext applicationContext, Class<?> beanClass, String scope) {
		String beanClassName = beanClass.getName();

		// 已注册跳过
		if (applicationContext.containsBean(beanClassName)) { return; }

		registerBean(applicationContext, beanClassName, scope);
	}

	/**
	 * @Title: 向spring容器注册bean
	 * @param applicationContext
	 * @param beanName
	 * @param scope 
	 */
	static void registerBean(ApplicationContext applicationContext, String beanName, String scope) {
		// ApplicationContext 不能为空
		Assert.notNull(applicationContext, "ApplicationContext can not be null");
		// scope 不能为空
		Assert.hasText(scope, "scope can not be blank");
		// beanClassName 不能为空
		Assert.hasText(beanName, "beanName can not be blank");
		// 断言 bean 没有定义
		Assert.state(!applicationContext.containsBean(beanName), "bean [" + beanName + "] is exist.");

		// configClass 本身是个 className
		BeanDefinition beanDefinition = genericBeanDefinition(beanName, scope);

		// 注册
		registerBeanDefinition(applicationContext, beanName, beanDefinition);
	}

	/**
	 * @Title: 生成 BeanDefinition
	 * @param beanClassName
	 * @param scope
	 * @return BeanDefinition
	 */
	static BeanDefinition genericBeanDefinition(String beanClassName, String scope) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(beanClassName);
		return builder.setScope(scope).getRawBeanDefinition();
	}

	/**
	 * @Title: 注册 bean 
	 * @param applicationContext
	 * @param beanName
	 * @param beanDefinition 
	 */
	static void registerBeanDefinition(ApplicationContext applicationContext, String beanName, BeanDefinition beanDefinition) {
		ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) applicationContext;
		BeanDefinitionRegistry beanDefinitonRegistry = (BeanDefinitionRegistry) configurableApplicationContext.getBeanFactory();
		beanDefinitonRegistry.registerBeanDefinition(beanName, beanDefinition);
	}
}
