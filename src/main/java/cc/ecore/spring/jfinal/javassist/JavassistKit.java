/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package cc.ecore.spring.jfinal.javassist;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

/**
 * Javassist 工具箱
 */
class JavassistKit {

	static ClassPool defaultPool;
	static {
		defaultPool = new ClassPool();
		defaultPool.insertClassPath(new ClassClassPath(JavassistKit.class));
	}

	// -------------------------------------------
	// get
	// -------------------------------------------
	public static CtClass get(String classname) {
		try {
			return defaultPool.get(classname);
		} catch (NotFoundException e) {
			throw new RuntimeException(classname + " is in a parent ClassPool.  Use the parent.");
		}
	}

	// -------------------------------------------
	// makeClass
	// -------------------------------------------
	public static CtClass makeClass(byte[] bytecode) {
		return makeClass(new ByteArrayInputStream(bytecode));
	}

	public static CtClass makeClass(InputStream classfile) {
		InputStream input = classfile;
		try {
			return defaultPool.makeClass(input);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// ignore Exception
				}
			}
		}
	}

	// -------------------------------------------
	// asArray
	// -------------------------------------------
	public static CtClass[] asArray(String... args) {
		int length = args.length;
		CtClass[] classes = new CtClass[length];
		for (int i = 0; i < length; i++) {
			classes[i] = get(args[i]);
		}
		return classes;
	}
}
