/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package cc.ecore.spring.jfinal.javassist;

import java.io.InputStream;

import javassist.CtClass;

/**
 * ClassFile 抽象类
 */
public abstract class AbstractClassFile {
	static String CLASS_SUFFIX = ".class";
	static ClassLoader loader;
	static {
		try {
			loader = Class.forName("com.jfinal.core.Const").getClassLoader();
		} catch (Exception e) {
			throw new RuntimeException("Can not exists of class: com.jfinal.core.Const", e);
		} 
	}
	
	protected final String classname;

	public AbstractClassFile(String classname) {
		this.classname = classname;
		if (classname == null || "".equals(classname.trim())) {//
			throw new IllegalArgumentException("classname can not be blank");
		}
	}

	public CtClass getCtClass() throws Exception {
		InputStream classfile = loader.getResourceAsStream(classname.replaceAll("[.]", "/") + CLASS_SUFFIX);
		return JavassistKit.makeClass(classfile);
	}
}
