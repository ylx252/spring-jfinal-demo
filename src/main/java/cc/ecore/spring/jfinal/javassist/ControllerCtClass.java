/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package cc.ecore.spring.jfinal.javassist;

import java.lang.reflect.Modifier;

import javassist.CtClass;
import javassist.CtMethod;

/**
 * Controller class 重构
 */
public class ControllerCtClass extends AbstractClassFile {

	public ControllerCtClass(String classname) {
		super(classname);
	}

	@Override
	public CtClass getCtClass() throws Exception {
		CtClass controllerCtClass = super.getCtClass();
		CtMethod method = controllerCtClass.getDeclaredMethod("init"//
				, JavassistKit.asArray("javax.servlet.http.HttpServletRequest"//
						, "javax.servlet.http.HttpServletResponse"//
						, "java.lang.String"));
		// 更新 访问修饰符
		// void init(HttpServletRequest request, HttpServletResponse response, String urlPara) 
		method.setModifiers(Modifier.PUBLIC);
		return controllerCtClass;
	}
}