/**
 * 	The MIT License (MIT)

	Copyright (c) 2016-2016 d3leaf@126.com

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */
package cc.ecore.spring.jfinal.javassist;

import java.lang.reflect.Modifier;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

/**
 * JFinalclass 重构
 */
public class JFinalCtClass extends AbstractClassFile {

	public JFinalCtClass(String classname) {
		super(classname);
	}
 
	@Override
	public CtClass getCtClass() throws Exception {
		CtClass jfinalCtClas = super.getCtClass();
		CtMethod method = jfinalCtClas
				.getDeclaredMethod("init", JavassistKit.asArray("com.jfinal.config.JFinalConfig", "javax.servlet.ServletContext"));
		// 更新 访问修饰符
		method.setModifiers(Modifier.PUBLIC);
		method.instrument(new ExprEditor() {
			public void edit(MethodCall m) throws CannotCompileException {
				if (m.getClassName().equals("com.jfinal.core.Config") && m.getMethodName().equals("configJFinal")) {
					m.replace("{ $_ = $proceed($$) ; com.jfinal.core.ApplicationContextKit.registerController(); }");
				}
			}
		});
		return jfinalCtClas;
	}
}
